#!/usr/bin/env python

import pandas as pd
import numpy as np
import urllib2
import datetime as dt
from datetime import date
import matplotlib.pyplot as plt
from sklearn import datasets
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn import preprocessing
#import requests

# Perform logistic regression on the half-hourly prices of the S&P 500 stocks to predict whether or not to buy or sell a particular stock. Written by Altan Allawala.

def getTickers():
	return [line.strip() for line in open("SP500tickers.txt")]


def get_google_closing_prices(symbol, period, window):
    url_root = 'http://www.google.com/finance/getprices?i='
    url_root += str(period) + '&p=' + str(window)
    url_root += 'd&f=d,c&df=cpct&q=' + symbol
#    print url_root
    response = urllib2.urlopen(url_root)
    data = response.read().split('\n')
    #actual data starts at index = 7
    #first line contains full timestamp,
    #every other line is offset of period from timestamp
    parsed_data = []
    anchor_stamp = ''
    end = len(data)
    for i in range(7, end):
        cdata = data[i].split(',')
        if 'a' in cdata[0]:
            #first one record anchor timestamp
            anchor_stamp = cdata[0].replace('a', '')
            cts = int(anchor_stamp)
        else:
            try:
                coffset = int(cdata[0])
                cts = int(anchor_stamp) + (coffset * period)
                if (dt.datetime.fromtimestamp(float(cts)).time().hour != 9) & (dt.date.fromtimestamp(float(cts)) != date.today()): # Drop today's info because we want to predict it and don't includeany data from 9:30am
                    parsed_data.append((str(dt.date.fromtimestamp(float(cts))), str(dt.datetime.fromtimestamp(float(cts)).time()), float(cdata[1])))
            except:
                pass # for time zone offsets thrown into data
    df = pd.DataFrame(parsed_data)
    df.columns = ['Date', 'Time', 'Close']
    return df # This function has been adapted from code freely available at https://mktstk.com/2014/12/31/how-to-get-free-intraday-stock-data-with-python/. In addition to modifying this function for the specific requirements of this problem, I identified and fixed an error in it (the opening and closing prices were swapped).


def convertToFractionalValues(stock): # Pivot the dataframe and calculate the fractional changes at different times of the day
	# TWO WAYS OF REORDERING (PIVOTING) THE DATAFRAME
	#print stock.pivot_table(values='Close', rows='Date', cols='Time') # Method 1
	stock = stock.groupby(['Date', 'Time'])['Close'].aggregate('mean').unstack() # Method 2

	stock = stock.drop(['15:30:00'], axis=1) # Discard the closing time price for 3:30pm since it will not be used in the regression

	stockFractionalChange = stock.copy() # Create new dataframe for the new (fractional) values

	columnNames = ["C%d" % i for i in range(len(stock.columns)-1)] # Create column names
	columnNames[len(stock.columns)-2] = 'CFinal'

	for i in range(len(stock.columns)-1):
		stockFractionalChange[columnNames[i]] = pd.DataFrame(stock.iloc[:,i+1] / stock.iloc[:,i] - 1.)

	stockFractionalChange = stockFractionalChange.ix[:, 'C0':]

	# Convert final closing price for the day to a 0 (if stock price dropped compared to at 3pm) or 1 (otherwise)
	stockFractionalChange['CFinal'] = stockFractionalChange['CFinal'].where(stockFractionalChange['CFinal'] > 0, 0)
	stockFractionalChange['CFinal'] = stockFractionalChange['CFinal'].where(stockFractionalChange['CFinal'] == 0, 1)

	return stockFractionalChange.dropna() # Get rid of rows/days containing any NaN


def prepareDataframeForOneStock(ticker, period, window): # Get data for one stock and present it as a pivoted dataframe
	stock = get_google_closing_prices(ticker, period, window) # Half hourly windows of the closing price for the last 50 days
	stockFractionalChange = convertToFractionalValues(stock) # The closing prices as a fractional return relative to the previous window
	del stock
	return stockFractionalChange


def prepareDataframeForAllStocks(tickers, period, window, save): # Get data for all stocks and combine into a single dataframe
	allStocks = pd.DataFrame()
	for t in tickers:#[0:2]:
		allStocks = allStocks.append(prepareDataframeForOneStock(t, period, window), ignore_index=True)
	
		if (tickers.index(t) % 50 == 0) and (tickers.index(t) != 0):
			print "	.... completed " + str(tickers.index(t)) + " out of " + str(len(tickers)) + " stocks"

	if save:
		saveData(allStocks)

	return allStocks


def saveData(df):
	df.to_pickle("stocksData")


def loadData(df):
	df = pd.read_pickle("stocksData")
	return df


def logisticRegression(stockFractionalChange):
	# load the iris datasets
	dataset = datasets.load_iris()
	features = stockFractionalChange.ix[:, 'C0':'C9'].as_matrix()
	outcome = stockFractionalChange['CFinal'].as_matrix()

	#predictors = dataset.data[20:69] # DELETE THESE LINES WHEN ITS WORKING. FOR NOW IT USES OTHER DATA.
	#target = dataset.target[20:69]
	features = preprocessing.scale(features) # Normalize the feature matrix to have mean 0 and variance 1 
	print outcome

	# fit a logistic regression model to the data
	model = LogisticRegression(fit_intercept=False, C=1., class_weight=None, random_state=987)
	model.fit(features, outcome)
	print(model)

	# make predictions
	expected = outcome
	predicted = model.predict(features) # This is feeding it new data. Ensure it doesn't give all zeros as a sanity check.
	print predicted

	# summarize the fit of the model
	print(metrics.classification_report(expected, predicted))
	print(metrics.confusion_matrix(expected, predicted))


# Either load the already saved data or else access it via Google Finance
#tickers = getTickers()
#print "1) Downloading and preparing stock data for analysis"
#dataFinal = prepareDataframeForAllStocks(tickers, 1800, 50, save=False)

print "1) Loading stock data for analysis:"
dataFinal = loadData("stocksData")

#print dataFinal

print "2) Performing logistic regression on training set:"
logisticRegression(dataFinal)

#pd.set_option('display.max_rows', None)
