The NYSE is open for day trading from 9:30am to 4pm. A study by Thomson Reuters concluded that 58% of all volume on the NYSE occurs during the first and last hour of trading. These two observations have two different explanations:

1) The volatility in the first hour is a reaction to earnings announcements or pre-dawn news.

2) The volatility in the last hour is due to large institutions operate in the post-lunch session. There has been an explosion of leveraged exchange-traded funds. These are traded in the last hour of the day to leverage the movement of the day up to this time and for company to rebalance their portfolios (see reference below). So the movements in the last hour are highly dependent on the intra-day stock prices on a specific day (much more so than during the rest of the day).

The included file "Volume time series.pdf" illustrates this trend, where the volume of transacted shares periodically rises at the end or beginning of the trading day.

The aim of this trading strategy is to perform logistic regression on the stock prices up to 3pm (sampled, say, every 30 minutes) as a predictor for the price of the stock at 4pm. A rise or fall in price is treated as a success or failure event respectively. If the regressor predicts a rise between 3 to 4pm, then the specific stock can be bought, held for one hour, and then sold.

By using the intraday stock price to find statistical patterns in the stock prices, and by automating this information processing step, the intention is to beat the market (i.e. at 3pm exactly) before non-automated players make their decisions.

Note that by only holding the stock for one hour, the associated market risks are significantly mitigated, resulting in a better Sharpe ratio. And by selling the stock before closing time, a player will not be exposed to after-hours or pre-dawn trading movements (during which time new information gets injected into the market).

http://money.cnn.com/2011/11/08/markets/leveraged_etfs/

This strategy I have been developing in my spare time during my PhD in theoretical physics at Brown University. It has been designed, coded and (slightly) tested by me, unless where otherwise noted.

Copyright (c) 2016 Altan Allawala